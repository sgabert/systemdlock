# SystemDLock

Copyright (C) 2016 Stephan Gabert (stephan dot gabert at fau dot de)

SystemD-Service for locking the screen on suspending.  
This is ideally used in a setup without an extra settings daemon (like GNOME's).

## Dependencies

* [scrot](http://linuxbrit.co.uk/scrot)
* [i3lock](http://i3wm.org/i3lock/)
* [ImageMagick/convert](https://www.imagemagick.org/script/convert.php)

## Preparation

* Configure systemd's logind (**/etc/systemd/logind.conf**):

    ```
    HandlePowerKey=suspend
    ```  
    ```
    HandleLidSwitch=ignore
    ```

* Restart the daemon:

    ```
    $ sudo service systemd-logind restart
    ```

## Installation

* Install:

    ```
    $ sudo make install
    ```

* Uninstall:

    ```
    $ sudo make uninstall
    ```

* Reload the systemd-Daemon and enable the service:

    ```
    $ sudo systemctl daemon-reload
    $ sudo systemctl enable lock-on-suspend.service
    ```

## Usage

* Get status:

    ```
    $ systemctl status lock-on-suspend.service
    ```

* Get general systemd-journal:

    ```
    $ sudo journalctl -b
    ```

* Get the unit's journal:

    ```
    $ sudo journalctl -b -u lock-on-suspend.service
    ```

## License

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
