#!/bin/bash

# scrot needs this to be a PNG - otherwise things get painful
tmp_file="$(tempfile --suffix '.png')"
rm -f $tmp_file

scrot $tmp_file

convert -blur 0x5 $tmp_file $tmp_file
sudo -u feni i3lock -i $tmp_file
rm -f $tmp_file
