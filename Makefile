SCRIPT_LOCAL=lock.sh
SCRIPT_TARGET=$(basename $(SCRIPT_LOCAL))
SCRIPT_DESTDIR=/usr/local/bin

SERVICE_FILE=lock-on-suspend.service
SERVICE_FILE_DESTDIR=/usr/local/lib/systemd/system
SYSTEMDDIR=/etc/systemd/system

RM=rm -f

.PHONY: help install uninstall

help:
	@echo "[help] possible targets:"
	@echo " install:   cp the script into its destination directory"
	@echo " uninstall: rm the installed systemd-service file and the script"
	@echo ""

install:
	@echo "[install] creating $(SCRIPT_DESTDIR)/$(SCRIPT_TARGET)"
	@cp "$(SCRIPT_LOCAL)" "$(SCRIPT_DESTDIR)/$(SCRIPT_TARGET)"
	@chmod 755 "$(SCRIPT_DESTDIR)/$(SCRIPT_TARGET)"
	@mkdir -p "$(SERVICE_FILE_DESTDIR)"
	@echo "[install] creating $(SERVICE_FILE_DESTDIR)/$(SERVICE_FILE)"
	@cp "$(SERVICE_FILE)" "$(SERVICE_FILE_DESTDIR)/$(SERVICE_FILE)"
	@chown root:root "$(SERVICE_FILE_DESTDIR)/$(SERVICE_FILE)"
	@echo "[install] create symlink $(SYSTEMDDIR)/$(SERVICE_FILE)"
	@ln -s "$(SERVICE_FILE_DESTDIR)/$(SERVICE_FILE)" \
		"$(SYSTEMDDIR)/$(SERVICE_FILE)"
	@echo "[install] success"

uninstall:
	$(RM) "$(SCRIPT_DESTDIR)/$(SCRIPT_TARGET)"
	$(RM) "$(SERVICE_FILE_DESTDIR)/$(SERVICE_FILE)"
	$(RM) "$(SYSTEMDDIR)/$(SERVICE_FILE)"
